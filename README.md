# Description
Création d'une application flask, création d'une image docker de cette application, stockage de cette image dans dockerHub, puis appel de cette application via curl
# Installation
```bash
pip install virtualenv
virtualenv <nom_virtualenv>
cd <nom_virtualenv>\script\activate
pip install -r requirements.txt
```

# Création de l'image de mon application

Permet de créer une image grâce à mon fichier Dockerfile et de lui donner un nom.
```bash
$ docker build -t my_docker_flask:latest . 
``` 
option -d : permet de laisser tourner mon container en arrière plan // -p : permet de choisir le port ici 5000.
```bash
$ docker run -d -p 5000:5000 my_docker_flask:latest  
```
# Commandes supplémentaires :

Permet de voir les images contenus dans mon docker. 
```bash
$docker images:
 ```
Permet de voir les mes container actifs.
```bash
$docker ps
``` 

Permet de kill un container via son id.
```bash
$docker kill <id_instance>
``` 
Permet de renommer le nom de l'image.
```bash
docker tag my_docker_flask mtngt/my_docker_flask
``` 
Supprime toutes instances locales docker. 
```bash
docker system prune -a
```
# Docker Hub :

Connexion à dockerhub.
```bash
$ docker login -u <login>
```
Permet de pusher mon image dans dockerhub (ici titio78/my_docker_flask est le nom de mon image).
```bash
$ docker push titio78/my_docker_flask
```
# Curl :
Permet de call mon api (le container doit tourner en même temps).
```bash
$ curl http://127.0.0.1:5000
``` 